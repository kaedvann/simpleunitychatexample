﻿using Moq;
using NUnit.Framework;
using SimleIoCTutorial.Services.Interfaces;
using SimleIoCTutorial.Utils;
using SimleIoCTutorial.ViewModels;

namespace TestTutorial
{
    public class TestExample
    {
        [Test]
        public void TestIncomingMessages()
        {
            var clientMocker = new Mock<INetworkClient>();
            var viewModel = new MainViewModel(clientMocker.Object);
            clientMocker.Raise(x => x.MessageRecieved += null, new MessageEventArgs(){Message = "hey"});
            clientMocker.Raise(x => x.MessageRecieved += null, new MessageEventArgs() { Message = "you" });
            Assert.AreEqual(2, viewModel.Messages.Count);
        }
    }
}
