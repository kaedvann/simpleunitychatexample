﻿using System;

namespace SimleIoCTutorial.Utils
{
    public class MessageEventArgs: EventArgs
    {
        public string Message { get; set; }
    }
}
