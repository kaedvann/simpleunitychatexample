﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SimleIoCTutorial.Annotations;
using SimleIoCTutorial.Services.Interfaces;
using SimleIoCTutorial.Utils;

namespace SimleIoCTutorial.ViewModels
{
    public class MainViewModel: INotifyPropertyChanged
    {
        private readonly INetworkClient _networkClient;
        private string _newMessage = "";

        public string NewMessage    
        {
            get { return _newMessage; }
            set
            {
                _newMessage = value;
                OnPropertyChanged("NewMessage");
                SendCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<string> Messages { get; set; } 

        public DelegateCommand SendCommand { get; set; }

        public MainViewModel(INetworkClient networkClient)
        {
            _networkClient = networkClient;
            _networkClient.MessageRecieved += (sender, args) => Messages.Add(args.Message);
            Messages = new ObservableCollection<string>();
            SendCommand = new DelegateCommand(SendMessage, () => NewMessage != "");
        }

        private void SendMessage()
        {
            _networkClient.SendMessage(NewMessage);
            Messages.Add(_networkClient.Login + ": " + NewMessage);
            NewMessage = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}