﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SimleIoCTutorial.Annotations;
using SimleIoCTutorial.Services.Interfaces;
using SimleIoCTutorial.Utils;

namespace SimleIoCTutorial.ViewModels
{
    public class LoginViewModel: INotifyPropertyChanged
    {
        private readonly IWindowController _controller;
        private string _login = "";
        
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged();
                AcceptCommand.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand AcceptCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        [Microsoft.Practices.Unity.Dependency]
        public INetworkClient Client { get; set; }

        public LoginViewModel(IWindowController controller)
        {
            _controller = controller;
            AcceptCommand = new DelegateCommand(Connect, () => Login != "");
            CancelCommand = new DelegateCommand(_controller.Shutdown);
        }

        private void Connect()
        {
            Client.Login = Login;
            Client.Connect();
            _controller.ShowMain();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
