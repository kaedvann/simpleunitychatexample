﻿using System.Windows;
using Microsoft.Practices.Unity;
using SimleIoCTutorial.Services.Implementations;
using SimleIoCTutorial.Services.Interfaces;

namespace SimleIoCTutorial
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private readonly IUnityContainer _container = new UnityContainer();

        public App()
        {
            FrameworkCompatibilityPreferences.KeepTextBoxDisplaySynchronizedWithTextProperty = true;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            
            WireContainer();

            _container.Resolve<IWindowController>().ShowLogin();
        }

        private void WireContainer()
        {
            _container.RegisterType<ILogger, DebugLogger>(new ContainerControlledLifetimeManager());
            _container.RegisterType<INetworkSettingsProvider, DummyNetworkSettingsProvider>(new ContainerControlledLifetimeManager());
            _container.RegisterType<INetworkClient, DummyNetworkClient>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IWindowController, WindowController>(new ContainerControlledLifetimeManager());
            _container.RegisterInstance(_container);
        }
    }
}
