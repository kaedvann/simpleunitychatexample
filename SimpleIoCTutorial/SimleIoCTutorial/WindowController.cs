﻿using System.Windows;
using Microsoft.Practices.Unity;
using SimleIoCTutorial.Views;

namespace SimleIoCTutorial
{
    public class WindowController: IWindowController
    {
        [Dependency]
        public IUnityContainer Container { get; set; }

        private Window _currentView = new Window();

        public void ShowLogin()
        {
            if (_currentView is LoginWindow) return;
            var previousView = _currentView;
            _currentView = Container.Resolve<LoginWindow>();
            _currentView.Show();
            previousView.Close();
        }

        public void ShowMain()
        {
            if (_currentView is MainWindow) return;
            var previousView = _currentView;
            _currentView = Container.Resolve<MainWindow>();
            _currentView.Show();
            previousView.Close();
        }

        public void Shutdown()
        {
            _currentView.Close();
        }
    }
}
