﻿using System;
using SimleIoCTutorial.Utils;

namespace SimleIoCTutorial.Services.Interfaces
{
    public interface INetworkClient
    {
        string Login { get; set; }

        void SendMessage(string message);
        
        void Connect();

        event EventHandler<MessageEventArgs> MessageRecieved;
    }
}
