﻿using System.Net;

namespace SimleIoCTutorial.Services.Interfaces
{
    public interface INetworkSettingsProvider
    {
        IPAddress IP { get; set; }
    }
}
