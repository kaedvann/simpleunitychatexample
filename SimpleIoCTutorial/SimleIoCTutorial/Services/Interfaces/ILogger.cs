﻿namespace SimleIoCTutorial.Services.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
    }
}
