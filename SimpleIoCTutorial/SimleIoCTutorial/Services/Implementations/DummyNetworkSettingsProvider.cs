﻿using System.Net;
using SimleIoCTutorial.Services.Interfaces;

namespace SimleIoCTutorial.Services.Implementations
{
    public class DummyNetworkSettingsProvider: INetworkSettingsProvider
    {
        private IPAddress _ip = IPAddress.Parse("127.0.0.1");

        public IPAddress IP
        {
            get { return _ip; }
            set { _ip = value; }
        }
    }
}
