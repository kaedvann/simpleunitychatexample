﻿using System.Diagnostics;
using SimleIoCTutorial.Services.Interfaces;

namespace SimleIoCTutorial.Services.Implementations
{
    public class DebugLogger: ILogger
    {
        public void Log(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
