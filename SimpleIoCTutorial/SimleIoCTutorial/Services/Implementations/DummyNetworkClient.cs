﻿using System;
using Microsoft.Practices.Unity;
using SimleIoCTutorial.Services.Interfaces;
using SimleIoCTutorial.Utils;

namespace SimleIoCTutorial.Services.Implementations
{
    public class DummyNetworkClient: INetworkClient
    {
        private readonly ILogger _logger;

        [Dependency]
        public INetworkSettingsProvider NetworkSettings { get; set; }

        public DummyNetworkClient(ILogger logger)
        {
            _logger = logger;
        }

        public string Login { get; set; }
        public void SendMessage(string message)
        {
            _logger.Log("sending message: "+message);
        }

        public void Connect()
        {
            if (NetworkSettings.IP == null)
                throw new ArgumentException("Can't connect without IP");
        }

        public event EventHandler<MessageEventArgs> MessageRecieved;
    }
}
