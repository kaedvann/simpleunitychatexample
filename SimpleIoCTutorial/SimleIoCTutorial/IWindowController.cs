﻿namespace SimleIoCTutorial
{
    public interface IWindowController
    {
        void ShowLogin();
        void ShowMain();
        void Shutdown();
    }
}